# nettle-sys

Low-level Rust bindings for the [Nettle cryptographic library](https://git.lysator.liu.se/nettle/nettle).

```toml
# Cargo.toml
[dependencies]
nettle-sys = "1.0"
```

The documentation can be found [here](https://sequoia-pgp.gitlab.io/nettle-sys).

## Cross compilation

`nettle-sys` can be cross compiled using [`cross`](https://github.com/rust-embedded/cross) and a custom Docker container. First, build the container and install `cross`:

```bash
cargo install cross
docker -t nettle-sys/<toolchain>:1 docker/<toolchain>
```

Then, you can cross compile the project:

```bash
cross --target <toolchain> -v
```

The build artifacts will be placed in `target/debug/<toolchain>`.

# License

This project is licensed under either of

 * GNU General Public License, Version 2.0, ([LICENSE-GPL2](LICENSE-GPL2) or
   https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * GNU General Public License, Version 3.0, ([LICENSE-GPL3](LICENSE-GPL3) or
   https://www.gnu.org/licenses/gpl.html)
 * GNU Lesser General Public License, Version 3.0, ([LICENSE-LGPL3](LICENSE-LGPL3) or
   https://www.gnu.org/licenses/lgpl.html)

at your option.
